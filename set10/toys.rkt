#lang racket
(require rackunit)
(require "extras.rkt")
(require 2htdp/universe) 
(require 2htdp/image)

(provide World%)
(provide SquareToy%)
(provide CircleToy%)
(provide make-world)
(provide run)
(provide make-square-toy)
(provide make-circle-toy)
(provide StatefulWorld<%>)
(provide StatefulToy<%>)

; The scene has a circular target
; The target is draggable
; Press "s" to create a new square toy
; Press "c" to create a new circle toy
; Square toys move horizontally at a constant rate bouncing off the sides
; Circular toys do not move, but they alternate between solid red and solid 
; green every 5 ticks
; run as (run 0.25 5) 

;------------------------------------------------------------------------------;
;                                  CONSTANTS                                   ;
;------------------------------------------------------------------------------;

; canvas definitions
(define CANVAS-WIDTH 400)
(define CANVAS-HEIGHT 500)
(define EMPTY-CANVAS (empty-scene CANVAS-WIDTH CANVAS-HEIGHT))
(define HALF-CANVAS-WIDTH (/ CANVAS-WIDTH 2))
(define HALF-CANVAS-HEIGHT (/ CANVAS-HEIGHT 2))

; colors
(define RED "red")
(define GREEN "green")
(define BLUE "blue")
(define BLACK "black")

; image modes
(define OUTLINE "outline")
(define SOLID "solid")

; initial values for target
(define TARGET-INITIAL-X (/ CANVAS-WIDTH 2))
(define TARGET-INITIAL-Y (/ CANVAS-HEIGHT 2))

; direction constants
(define LEFT "left")
(define RIGHT "right")

; mouse events
(define BUTTON-DOWN "button-down")
(define BUTTON-UP "button-up")
(define DRAG "drag")

; constants
(define ZERO 0)
(define ONE 1)

; side of the square
(define SQUARE-TOY-SIDE 40)

; radius of the circle toy
(define CIRCLE-TOY-RADIUS 5)

; boundaries
(define LEFT-TANGENT (+ 0 (/ SQUARE-TOY-SIDE 2)))
(define RIGHT-TANGENT (- CANVAS-WIDTH (/ SQUARE-TOY-SIDE 2)))

; test constants
(define TEN 10)
;------------------------------------------------------------------------------;
;                              DATA DEFINITIONS                                ;
;------------------------------------------------------------------------------;
; A Direction is one of 
; -- "right"  INTERP: square moving rightward
; -- "left"   INTERP: square moving leftward
; TEMPLATE:
; direction-fn : Direction -> ??
;(define (direction-fn dir)
;  (cond
;    [(string=? dir LEFT) ...]
;    [(string=? dir RIGHT) ...]))
;----------------------------------------------;
; A ColorString is one of
; -- "red"    INTERP: the color red
; -- "green"  INTERP: the color green
; -- "blue"   INTERP: the color blue
; -- "black"  INTERP: the color black
; TEMPLATE:
; color-fn : ColorString -> ??
;(define (color-fn c)
;  (cond
;    [(string=? c "red") ...]
;    [(string=? c "green") ...]
;    [(string=? c "blue") ...]
;    [(string=? c "black") ...]))
;----------------------------------------------;
; A ListOfStatefulToy<%> (LOT) is one of
; -- empty
; -- (cons StatefulToy<%> ListOfStatefulToy<%>)
; WHERE: a StatefulToy<%> may be a CircleToy% or a SquareToy%
; TEMPLATE:
; lot-fn : LOT -> ??
;(define (lot-fn lot)
;  (cond
;    [(empty? lot) ...]
;    [else ...
;     (send (first lot) stateful-toy<%>-fn)
;     (lot-fn (rest lot))]))
;----------------------------------------------;
; A ListOfCircleToy% (LOC) is one of
; -- empty
; -- (cons CircleToy% ListOfCircleToy%)
; TEMPLATE:
; loc-fn : LOC -> ??
;(define (loc-fn loc)
;  (cond
;    [(empty? loc) ...]
;    [else ...
;     (send (first loc) stateful-toy<%>-fn)
;     (loc-fn (rest loc))]))
;----------------------------------------------;
; A ListOfSquareToy% (LOS) is one of
; -- empty
; -- (cons SquareToy% ListOfSquareToy%)
; TEMPLATE:
; los-fn : LOS -> ??
;(define (los-fn los)
;  (cond
;    [(empty? los) ...]
;    [else ...
;     (send (first los) stateful-toy<%>-fn)
;     (los-fn (rest los))]))
;------------------------------------------------------------------------------;
;                                  INTERFACE                                   ;
;------------------------------------------------------------------------------;
(define StatefulWorld<%>
  (interface ()
    
    ; -> Void
    ; GIVEN: no arguments
    ; EFFECT: updates this StatefulWorld<%> to the state that it 
    ;          should be in after a tick.
    on-tick                             
    
    ; Integer Integer MouseEvent -> Void
    ; GIVEN:  a location and a mouse event
    ; EFFECT: updates this StatefulWorld<%> to the state that it should be in
    ;          after the given MouseEvent
    on-mouse
    
    ; KeyEvent -> Void
    ; GIVEN: a key event
    ; EFFECT: updates this StatefulWorld<%> to the state that it should be in
    ;          after the given KeyEvent
    on-key
    
    ; -> Scene
    ; GIVEN: no arguments
    ; RETURNS: a Scene depicting this StatefulWorld<%>
    on-draw 
    
    ; -> Integer
    ; GIVEN: no arguments
    ; RETURN: the x and y coordinates of the target
    target-x
    target-y
    
    ; -> Boolean
    ; GIVEN: no arguments
    ; RETURNS: true if the target is selected else false
    target-selected?
    
    ; -> ListOfStatefulToy<%>
    ; GIVEN: no arguments
    ; RETURNS: a list of toys in this StatefulWorld<%>
    get-toys
    
    ;-------------------------------------------------------------;
    ; methods defined for tests
    ;-------------------------------------------------------------;
    ; -> PosInt
    ; GIVEN: no arguments
    ; RETURNS: the speed of this StatefulWorld<%>
    get-speed
    
    ; -> Integer
    ; GIVEN: no arguments
    ; RETURNS: x position of the mouse inside the target
    ;          when selected
    get-target-mx
    
    ; -> Integer
    ; GIVEN: no arguments
    ; RETURNS: y position of the mouse inside the target
    ;          when selected 
    get-target-my))
;------------------------------------------------------------------------------;
(define StatefulToy<%> 
  (interface ()
    
    ; -> Void
    ; GIVEN: no arguments
    ; EFFECT: updates this StatefulToy<%> to the state it should be in after a
    ;         tick. 
    on-tick                             
    
    ; Scene -> Scene
    ; GIVEN: a Scene
    ; RETURNS: a Scene like the given one, but with this StatefulToy<%> drawn
    ;          on it.
    add-to-scene
    
    ; -> Int
    ; GIVEN: no arguments
    ; RETURNS: the x and y positions of the center of this StatefulToy<%>.
    toy-x
    toy-y
    
    ; -> ColorString
    ; GIVEN: no arguments
    ; RETURNS: the current color of this StatefulToy<%>
    toy-color))
;------------------------------------------------------------------------------;
;                                   CLASSES                                    ;
;------------------------------------------------------------------------------;
; World%     -- a class that satisfies the StatefulWorld<%> interface
; A World is a (new World% [toys ListOfStatefulToy<%>]
;                          [speed   PosInt]
;                          [x       Integer]
;                          [y       Integer]
;                          [selected? Boolean]
;                          [target-mx Integer]
;                          [target-my Integer])
; Interpretation: represents a world, containing a target and some circular
;                 and square toys where
; toys: a list of toys in the world
; speed: speed of toys (in pixels/tick)
; x: the x position of the target 
; y: the y position of the target 
; selected?: whether the target is selected
; target-x: x position of the mouse inside the target image when selected
; target-y: y position of the mouse inside the target image when selected

(define World%
  (class* object% (StatefulWorld<%>)
    (init-field toys)        ; a list of toys in the game
    (init-field speed)       ; speed of toys (in pixels/tick)
    (init-field x)           ; the x position of the target 
    (init-field y)           ; the y position of the target
    (init-field selected?)   ; whether the target is selected
    (init-field target-mx)   ; x position of the mouse 
    ;                          inside the target image when selected 
    (init-field target-my)   ; y position of the mouse 
    ;                          inside the target image when selected
    
    ; Target Image radius
    (field [TARGET-RADIUS 10])
    ; Target color
    (field [TARGET-COLOR BLACK])
    ; image for displaying the target
    (field [TARGET-IMG (circle TARGET-RADIUS OUTLINE TARGET-COLOR)]) 
    
    (super-new)
    
    ; on-tick: -> Void
    ; GIVEN: no arguments
    ; EFFECT: updates this StatefulWorld<%> to the state that it should be in 
    ;         after a tick.
    ; EXAMPLES:
    ; circle1 -> (new CircleToy% [x 100] [y 200] [color GREEN] [tick-count 5])
    ; square1 -> (new SquareToy% [x 100] [y 300] [d LEFT] [speed 5])
    ; list-of-toys-1 -> (list circle1 square1)
    ; world-1 -> (new World%
    ;               [toys list-of-toys-1] [speed 5]
    ;               [x 200] [y 250] [selected? false] 
    ;               [target-mx 0] [target-my 0])
    ; (send world-1 on-tick) -> updates the given stateful world such that the 
    ;                           tick-count of circle1 is set to 1 and color 
    ;                           is changed to RED. The square toy moves left
    ;                           by 5 pixels
    (define/public (on-tick)
      (for-each 
       ; Toy<%> -> Toy<%>
       ; GIVEN: a toy
       ; RETURNS: the toy after a tick
       (lambda (toy) (send toy on-tick))
       toys))
    
    ; on-mouse: Integer Integer MouseEvent -> Void
    ; GIVEN:  a location and a mouse event
    ; EFFECT: updates this StatefulWorld<%> to the state that it should be in
    ;         after the given MouseEvent
    ; EXAMPLES:
    ; square1 -> (new SquareToy% [x 200] [y 250] [d RIGHT] [speed 5])
    ; list-of-toys-1 -> (list square1)
    ; world-1 -> (new World%
    ;               [toys list-of-toys-1] [speed 5]
    ;               [x 200] [y 250] [selected? false] 
    ;               [target-mx 0] [target-my 0])
    ; world-2 -> (new World%
    ;               [toys list-of-toys-1] [speed 5]
    ;               [x 200] [y 250] [selected? true] 
    ;               [target-mx 205] [target-my 245])
    ; (send world-1 on-mouse 205 245 BUTTON-DOWN) -> the given stateful world 
    ;               is updated with the target selected.
    ; (send world-2 on-mouse 205 245 BUTTON-UP) -> the given stateful world
    ;               is updated with the target unselected.
    ; world-3 -> (new World%
    ;               [toys list-of-toys-1] [speed 5]
    ;               [x 200] [y 250] [selected? true] 
    ;               [target-mx 205] [target-my 245])
    ; (send world-3 on-mouse 205 245 DRAG) -> the given stateful world is
    ;               updated with the new coordinates after the drag.
    ;               Below are the changed fields with their new values:
    ;               [x 95] [y 105] [target-mx 100] [target-my 100])
    ; STRATEGY: Cases on mev: MouseEvent
    ; DETAILS: Target gets selected on a button-down event inside it
    ;          Target gets unselected on a button-up event inside it
    ;          Target gets dragged on a drag event if the mouse is inside it
    ;          all other mouse events are ignored
    (define/public (on-mouse mx my mev)
      (cond
        [(mouse=? mev BUTTON-DOWN) (send this world-after-button-down mx my)]
        [(mouse=? mev BUTTON-UP) (send this world-after-button-up)]
        [(mouse=? mev DRAG) (send this world-after-drag mx my)]
        [else this]))
    
    ; world-after-button-down: Integer Integer -> Void
    ; GIVEN: x and y positions of the mouse pointer
    ; EFFECT: the target in StatefulWorld<%> is selected if the mouse
    ;         pointer is in it.
    ; EXAMPLES:
    ; square1 -> (new SquareToy% [x 200] [y 250] [d RIGHT] [speed 5])
    ; list-of-toys-1 -> (list square1)
    ; world-1 -> (new World%
    ;               [toys list-of-toys-1] [speed 5]
    ;               [x 200] [y 250] [selected? false] 
    ;               [target-mx 0] [target-my 0])
    ; (send world-1 world-after-button-down 205 245) -> the given stateful world
    ;               is updated so that the target is selected, and the 
    ;               position of the mouse inside the target is assigned the
    ;               given mouse coordinates.
    ;               Below are the changed fields with their new values
    ;               [selected? true] [target-mx 205] [target-my 245])
    (define/public (world-after-button-down mx my)
      (begin 
        (set! selected? (send this in-target? mx my))
        (set! target-mx mx)
        (set! target-my my)))
    
    ; in-target?: Integer Integer -> Boolean
    ; GIVEN: x and y positions of the mouse pointer
    ; RETURNS: true iff the mouse pointer is in the target
    ; EXAMPLES:
    ; square1 -> (new SquareToy% [x 200] [y 250] [d RIGHT] [speed 5])
    ; list-of-toys-1 -> (list square1)
    ; world-1 -> (new World%
    ;               [toys list-of-toys-1] [speed 5]
    ;               [x 200] [y 250] [selected? false] 
    ;               [target-mx 0] [target-my 0])
    ; (send world-1 in-target? 205 245) -> true
    (define/public (in-target? mx my)
      (<=
       (+ (sqr (- mx x)) (sqr (- my y)))
       (sqr TARGET-RADIUS)))
    
    ; world-after-button-up: Integer Integer -> Void
    ; GIVEN: x and y positions of the mouse pointer
    ; EFFECT: the target in StatefulWorld<%> is unselected if the
    ;         mouse is in it
    ; EXAMPLES:
    ; square1 -> (new SquareToy% [x 200] [y 250] [d RIGHT] [speed 5])
    ; list-of-toys-1 -> (list square1)
    ; world-1 -> (new World%
    ;               [toys list-of-toys-1] [speed 5]
    ;               [x 200] [y 250] [selected? true] 
    ;               [target-mx 205] [target-my 245])
    ; (send world-1 world-after-button-up 205 245) -> the given stateful world
    ;               is updated such that the target is unselected as the mouse
    ;               is in it.
    (define/public (world-after-button-up)
      (if selected?
          (set! selected? false)
          this))
    
    ; world-after-drag: Integer Integer -> Void
    ; GIVEN: x and y positions of the mouse pointer
    ; EFFECT: make the target drag along with the mouse if the mouse is
    ;         in the target
    ; EXAMPLES:
    ; square1 -> (new SquareToy% [x 200] [y 250] [d RIGHT] [speed 5])
    ; list-of-toys-1 -> (list square1)
    ; world-1 -> (new World%
    ;               [toys list-of-toys-1] [speed 5]
    ;               [x 200] [y 250] [selected? true] 
    ;               [target-mx 205] [target-my 245])
    ; (send world-1 world-after-drag 100 100) -> the given stateful world is
    ;               updated with the new coordinates after the drag.
    ;               Below are the changed fields with their new values:
    ;               [x 95] [y 105] [target-mx 100] [target-my 100])
    (define/public (world-after-drag mx my)
      (if selected?
          (begin
            (set! x (send this new-x mx))
            (set! y (send this new-y my))
            (set! target-mx mx)
            (set! target-my my))
          this))
    
    ; new-x: Integer -> Integer
    ; GIVEN: x position of the mouse pointer
    ; RETURNS: the new x position of the center of the target after the drag
    ; EXAMPLES:
    ; square1 -> (new SquareToy% [x 200] [y 250] [d RIGHT] [speed 5])
    ; list-of-toys-1 -> (list square1)
    ; world-1 -> (new World%
    ;               [toys list-of-toys-1] [speed 5]
    ;               [x 200] [y 250] [selected? true] 
    ;               [target-mx 205] [target-my 245])
    ; (send world-1 new-x 100) -> (+ 200 (- 100 205)) -> 95
    (define/public (new-x mx)
      (+ x (- mx target-mx)))
    
    ; new-y: Integer -> Integer
    ; GIVEN: y position of the mouse pointer
    ; RETURNS: the new y position of the center of the target after the drag
    ; EXAMPLES:
    ; square1 -> (new SquareToy% [x 200] [y 250] [d RIGHT] [speed 5])
    ; list-of-toys-1 -> (list square1)
    ; world-1 -> (new World%
    ;               [toys list-of-toys-1] [speed 5]
    ;               [x 200] [y 250] [selected? true] 
    ;               [target-mx 205] [target-my 245])
    ; (send world-1 new-y 100) -> (+ 250 (- 100 245)) -> 105
    (define/public (new-y my)
      (+ y (- my target-my)))
    
    ; on-key: KeyEvent -> Void
    ; GIVEN: a key event
    ; EFFECT: updates this StatefulWorld<%> to the state that it should be in
    ;         after the given KeyEvent
    ; EXAMPLES:
    ; circle1 -> (new CircleToy% [x 200] [y 250] [color GREEN] [tick-count 0])
    ; square1 -> (new SquareToy% [x 200] [y 250] [d RIGHT] [speed 5])
    ; list-of-toys-1 -> empty
    ; world-1 -> (new World%
    ;               [toys list-of-toys-1] [speed 5]
    ;               [x 200] [y 250] [selected? false] 
    ;               [target-mx 0] [target-my 0])
    ; (send world-1 on-key c) -> the given stateful world is updated with
    ;               a circle toy added to its list of toys.
    ; (send world-1 on-key s) -> the given stateful world is updated with 
    ;               a square toy added to its list of toys.
    ; DETAILS: A circle toy is created on event "c"
    ;          A square toy is created on event "s"
    ;          all other key events are ignored
    ; STRATEGY: Cases on kev : KeyEvent
    (define/public (on-key kev)
      (cond
        [(key=? kev "c") (send this world-after-key-c)]
        [(key=? kev "s") (send this world-after-key-s)]
        [else this]))
    
    ; world-after-key-c : -> Void
    ; GIVEN: no arguments
    ; EFFECT: updates this StatefulWorld<%> to the state that it should be in
    ;         after the KeyEvent "c"
    ; EXAMPLES:
    ; circle1 -> (new CircleToy% [x 200] [y 250] [color GREEN] [tick-count 0])
    ; square1 -> (new SquareToy% [x 250] [y 350] [d RIGHT] [speed 5])
    ; list-of-toys-1 -> (list circle1 square1)
    ; world-1 -> (new World%
    ;               [toys list-of-toys-1] [speed 5]
    ;               [x 200] [y 250] [selected? false] 
    ;               [target-mx 0] [target-my 0])
    ; (send world-1 world-after-key-c) -> the given stateful world is updated
    ;            with a circle toy added to its list of toys.
    (define/public (world-after-key-c)
      (set! toys (cons (make-circle-toy 
                        x
                        y) toys)))
    
    ; world-after-key-s : -> Void
    ; GIVEN: no arguments
    ; EFFECT: updates this StatefulWorld<%> to the state that it should be in
    ;         after the KeyEvent "s"
    ; EXAMPLES:
    ; circle1 -> (new CircleToy% [x 100] [y 100] [color GREEN] [tick-count 0])
    ; square1 -> (new SquareToy% [x 200] [y 250] [d RIGHT] [speed 5])
    ; list-of-toys-1 -> (list circle1 square1)
    ; world-1 -> (new World%
    ;               [toys list-of-toys-1] [speed 5]
    ;               [x 200] [y 250] [selected? false] 
    ;               [target-mx 0] [target-my 0])
    ; (send world-1 world-after-key-s) -> the given stateful world is updated
    ;               with a square toy added to its list of toys
    (define/public (world-after-key-s)
      (set! toys (cons (make-square-toy
                        x
                        y
                        speed) toys)))
    
    ; on-draw: -> Scene
    ; GIVEN: no arguments
    ; RETURNS: a Scene depicting this StatefulWorld<%>
    ; EXAMPLES:
    ; circle1 -> (new CircleToy% [x 100] [y 100] [color GREEN] [tick-count 0])
    ; square1 -> (new SquareToy% [x 200] [y 300] [d RIGHT] [speed 5])
    ; list-of-toys-1 -> (list circle1 square1)
    ; world-1 -> (new World%
    ;               [toys list-of-toys-1] [speed 5]
    ;               [x 200] [y 250] [selected? false] 
    ;               [target-mx 0] [target-my 0])
    ; scene-with-target -> (place-image TARGET-IMG 200 250 EMPTY-CANVAS))
    ; scene-with-circle -> (place-image GREEN-CIRCLE-IMG 100 100 
    ;                                                    scene-with-target)
    ; scene-with-toys -> (place-image SQUARE-IMG 200 300 scene-with-circle)
    ; (send world-1 on-draw) -> scene-with-toys
    (define/public (on-draw)
      (local 
        ((define scene-with-target (place-image TARGET-IMG x y EMPTY-CANVAS))  
         (define scene-with-toys (send this toys-on-scene scene-with-target)))
        scene-with-toys))
    
    ; toys-on-scene: Scene -> Scene
    ; GIVEN: a scene
    ; RETURNS: the toys in this StatefulWorld<%> drawn on the given scene
    ; EXAMPLES:
    ; circle1 -> (new CircleToy% [x 100] [y 100] [color GREEN] [tick-count 0])
    ; square1 -> (new SquareToy% [x 200] [y 300] [d RIGHT] [speed 5])
    ; list-of-toys-1 -> (list circle1 square1)
    ; world-1 -> (new World%
    ;               [toys list-of-toys-1] [speed 5]
    ;               [x 200] [y 250] [selected? false] 
    ;               [target-mx 0] [target-my 0])
    ; scene-with-target -> (place-image TARGET-IMG 200 250 EMPTY-CANVAS))
    ; scene-with-circle -> (place-image GREEN-CIRCLE-IMG 100 100 
    ;                                                    scene-with-target)
    ; scene-with-toys -> (place-image SQUARE-IMG 200 300 scene-with-circle)
    ; (send world-1 toys-on-scene scene-with-target) -> scene-with-toys
    (define/public (toys-on-scene scene)
      (foldr
       ; Toy Scene -> Scene
       ; GIVEN: a square and a scene with squares drawn so far
       ; RETURNS: the given square drawn on the given scene
       (lambda (toy scene-so-far)
         (send toy add-to-scene scene-so-far))
       scene
       toys))
    
    ; target-x:  -> Integer
    ; target-y:  -> Integer
    ; GIVEN: no arguments
    ; RETURN: the x and y coordinates of the target of this StatefulWorld<%>
    ; EXAMPLES:
    ; circle1 -> (new CircleToy% [x 100] [y 100] [color GREEN] [tick-count 0])
    ; square1 -> (new SquareToy% [x 200] [y 300] [d RIGHT] [speed 5])
    ; list-of-toys-1 -> (list circle1 square1)
    ; world-1 -> (new World%
    ;               [toys list-of-toys-1] [speed 5]
    ;               [x 200] [y 250] [selected? false] 
    ;               [target-mx 0] [target-my 0])
    ; (send world-1 target-x) -> 200
    ; (send world-1 target-y) -> 250
    (define/public (target-x)
      x)
    (define/public (target-y)
      y)
    
    ; target-selected?: -> Boolean
    ; GIVEN: no arguments
    ; RETURNS: true if the target of this StatefulWorld<%> is selected 
    ;          else false
    ; EXAMPLES:
    ; circle1 -> (new CircleToy% [x 100] [y 100] [color GREEN] [tick-count 0])
    ; square1 -> (new SquareToy% [x 200] [y 300] [d RIGHT] [speed 5])
    ; list-of-toys-1 -> (list circle1 square1)
    ; world-1 -> (new World%
    ;               [toys list-of-toys-1] [speed 5]
    ;               [x 200] [y 250] [selected? false] 
    ;               [target-mx 0] [target-my 0])
    ; (send world-1 target-selected?) -> false
    (define/public (target-selected?)
      selected?)
    
    ; get-toys: -> ListOfToy<%>
    ; GIVEN: no arguments
    ; RETURNS: the list of toys of this StatefulWorld<%>
    ; EXAMPLES:
    ; circle1 -> (new CircleToy% [x 100] [y 100] [color GREEN] [tick-count 0])
    ; square1 -> (new SquareToy% [x 200] [y 300] [d RIGHT] [speed 5])
    ; list-of-toys-1 -> (list circle1 square1)
    ; world-1 -> (new World%
    ;               [toys list-of-toys-1] [speed 5]
    ;               [x 200] [y 250] [selected? false] 
    ;               [target-mx 0] [target-my 0])
    ; (send world-1 get-toys) -> list-of-toys-1
    (define/public (get-toys)
      toys)
    
    ; get-speed: -> PosInt
    ; GIVEN: no arguments
    ; RETURNS: the speed of this StatefulWorld<%>
    ; EXAMPLES:
    ; circle1 -> (new CircleToy% [x 100] [y 100] [color GREEN] [tick-count 0])
    ; square1 -> (new SquareToy% [x 200] [y 300] [d RIGHT] [speed 5])
    ; list-of-toys-1 -> (list circle1 square1)
    ; world-1 -> (new World%
    ;               [toys list-of-toys-1] [speed 5]
    ;               [x 200] [y 250] [selected? false] 
    ;               [target-mx 0] [target-my 0])
    ; (send world-1 get-speed) -> 5
    (define/public (get-speed)
      speed)
    
    ; get-target-mx: -> Integer
    ; GIVEN: no arguments
    ; RETURNS: x position of the mouse inside the target of this 
    ;          StatefulWorld<%> when selected 
    ; EXAMPLES:
    ; circle1 -> (new CircleToy% [x 100] [y 100] [color GREEN] [tick-count 0])
    ; square1 -> (new SquareToy% [x 200] [y 300] [d RIGHT] [speed 5])
    ; list-of-toys-1 -> (list circle1 square1)
    ; world-1 -> (new World%
    ;               [toys list-of-toys-1] [speed 5]
    ;               [x 200] [y 250] [selected? true] 
    ;               [target-mx 205] [target-my 245])
    ; (send world-1 get-target-mx) -> 205
    (define/public (get-target-mx)
      target-mx)
    
    ; get-target-my: -> Integer
    ; GIVEN: no arguments
    ; RETURNS: y position of the mouse inside the target of this 
    ;          StatefulWorld<%> when selected 
    ; EXAMPLES:
    ; circle1 -> (new CircleToy% [x 100] [y 100] [color GREEN] [tick-count 0])
    ; square1 -> (new SquareToy% [x 200] [y 300] [d RIGHT] [speed 5])
    ; list-of-toys-1 -> (list circle1 square1)
    ; world-1 -> (new World%
    ;               [toys list-of-toys-1] [speed 5]
    ;               [x 200] [y 250] [selected? true] 
    ;               [target-mx 205] [target-my 245])
    ; (send world-1 get-target-my) -> 245
    (define/public (get-target-my)
      target-my)))
;------------------------------------------------------------------------------;
; SquareToy% -- a class that satisfies the StatefulToy<%> interface
; A SquareToy is a (new SquareToy% [x Int] 
;                                  [y Int] 
;                                  [d Direction]
;                                  [speed PosInt])
; Interpretation: represents a square toy where
; x: x-position of the center of the square
; y: y-position of the center of the square
; d: direction in which the square toy is moving
; speed: speed at which the square toy is moving (in pixels/tick)
(define SquareToy%
  (class* object% (StatefulToy<%>)
    (init-field x)         ; the x-position of the center of the square toy
    (init-field y)         ; the y-position of the center of the square toy
    (init-field d)         ; the direction in which the square toy is moving
    (init-field speed)     ; the speed of square toy (in pixels/tick)
    
    ; color of the square toy
    (field [SQUARE-COLOR BLUE])
    ; image for displaying the square toy
    (field [SQUARE-IMG (square SQUARE-TOY-SIDE OUTLINE SQUARE-COLOR)])
    
    (super-new)
    
    ; on-tick : -> Void
    ; GIVEN: no arguments
    ; EFFECT: the state of this StatefulToy<%> that should follow a tick.
    ; square1 -> (new SquareToy% [x 100] [y 300] [d LEFT] [speed 5])
    ; (send square1 on-tick) -> the stateful toy moved to the left 
    ;                  by one speed.
    (define/public (on-tick)
      (if (send this square-inside?)
          (send this same-direction-move)
          (send this opp-direction-move)))
    
    ; square-inside? : -> Boolean
    ; GIVEN: no arguments
    ; RETURNS: true iff this StatefulToy<%> is inside the boundaries of
    ;          the canvas
    ; EXAMPLES:
    ; square1 -> (new SquareToy% [x 100] [y 300] [d LEFT] [speed 5])
    ; (send square1 square-inside?) -> true
    ; square2 -> (new SquareToy% [x 10] [y 300] [d LEFT] [speed 5])
    ; (send square2 square-inside?) -> false
    (define/public (square-inside?)
      (<= LEFT-TANGENT
          x
          RIGHT-TANGENT))
    
    ; same-direction-move: -> Void
    ; GIVEN: no arguments
    ; EFFECT: this StatefulToy<%> that has moved one tick in the same direction
    ; EXAMPLES:
    ; square1 -> (new SquareToy% [x 100] [y 300] [d LEFT] [speed 5])
    ; (send square1 same-direction-move) -> the stateful toy moved to the 
    ;             left by one speed.
    ; square2 -> (new SquareToy% [x 40] [y 300] [d RIGHT] [speed 5])
    ; (send square2 same-direction-move) -> the stateful toy moved to the
    ;             right by speed.
    ; STRATEGY: Structural decomposition on d: Direction
    (define/public (same-direction-move)
      (cond 
        [(string=? d LEFT) (send this move-to-left)]
        [(string=? d RIGHT) (send this move-to-right)]))
    
    ; move-to-left: -> Void
    ; GIVEN: no arguments
    ; EFFECT: this StatefulToy<%> moved to left by one tick if it has not 
    ;          crossed the boundary, else moved to the left boundary and 
    ;          direction changed to right
    ; EXAMPLES:
    ; square1 -> (new SquareToy% [x 100] [y 300] [d LEFT] [speed 5])
    ; (send square1 move-to-left) -> the stateful toy moved to the
    ;             left by speed.
    ; square2 -> (new SquareToy% [x 15] [y 300] [d LEFT] [speed 5])
    ; (send square2 move-to-left) -> the stateful toy moved to the left-boundary
    ;          and the direction changed to right as it goes beyond boundary
    (define/public (move-to-left)
      (if (>= (- x speed) LEFT-TANGENT)
          (set! x (- x speed))
          (begin 
            (set! x LEFT-TANGENT)
            (set! d (send this new-direction)))))
    
    ; move-to-right: -> Void
    ; GIVEN: no arguments
    ; EFFECT: this StatefulToy<%> moved to right by one tick if it has not 
    ;         crossed the boundary, else moved to the right boundary and 
    ;         direction changed to left
    ; EXAMPLES: 
    ; square1 -> (new SquareToy% [x 40] [y 300] [d RIGHT] [speed 5])
    ; (send square1 move-to-right) -> the stateful toy moved to the
    ;             right by speed.
    ; square2 -> (new SquareToy% [x 385] [y 300] [d RIGHT] [speed 5])
    ;(send square2 move-to-right)-> the stateful toy moved to the right-boundary
    ;          and the direction changed to left as it goes beyond boundary
    (define/public (move-to-right)
      (if (<= (+ x speed) RIGHT-TANGENT)
          (set! x (+ x speed))
          (begin 
            (set! x RIGHT-TANGENT)
            (set! d (send this new-direction)))))
    
    ; opp-direction-move: -> Void
    ; GIVEN: no arguments
    ; RETURNS: this StatefulToy<%> updated to move in the opposite direction
    ; EXAMPLES: 
    ; square1 -> (new SquareToy% [x 10] [y 300] [d LEFT] [speed 5])
    ; (send square1 opp-direction-move) -> the stateful toy moved to the 
    ;             left-boundary and its direction is changed to right.
    (define/public (opp-direction-move)
      (set! x (send this new-x))
      (set! d (send this new-direction)))
    
    ; new-x: -> Int
    ; GIVEN: no arguments
    ; RETURNS: the new x position of this StatefulToy<%> which is outside the 
    ;          boundaries
    ;          - it is LEFT-TANGENT if it is out on the left side
    ;          - it is RIGHT-TANGENT if it is out on the right side
    ; EXAMPLES:
    ; square1 -> (new SquareToy% [x 10] [y 300] [d LEFT] [speed 5])
    ; (send square1 new-x) -> 20 (LEFT-TANGENT)
    ; square2 -> (new SquareToy% [x 390] [y 300] [d RIGHT] [speed 5])
    ; (send square2 new-x) -> 380 (RIGHT-TANGENT)
    (define/public (new-x)
      (if (< x LEFT-TANGENT)
          LEFT-TANGENT
          RIGHT-TANGENT))
    
    ; new-direction: -> Direction
    ; GIVEN: no arguments
    ; RETURNS: the new direction of this StatefulToy<%> which is outside the 
    ;          boundaries
    ;          - it is "right" if it goes beyond the left boundary
    ;          - else it is "left" which means it has gone beyond the 
    ;            right boundary
    ; EXAMPLES:
    ; square1 -> (new SquareToy% [x 10] [y 300] [d LEFT] [speed 5])
    ; (send square1 new-direction) -> "right"
    (define/public (new-direction)
      (cond
        [(string=? d LEFT) RIGHT]
        [(string=? d RIGHT) LEFT]))
    
    ; add-to-scene: Scene -> Scene
    ; GIVEN: a scene
    ; RETURNS: a Scene like the given one, but with StatefulToy<%> object
    ;          drawn on it.
    ; EXAMPLES:
    ; square1 -> (new SquareToy% [x 200] [y 300] [d RIGHT] [speed 5])
    ; scene1 -> (place-image GREEN-CIRCLE-IMG 100 200 EMPTY-CANVAS)
    ; scene2 -> (place-image SQUARE-IMG 200 300 scene1)
    ; (send square1 add-to-scene scene1) -> scene2
    (define/public (add-to-scene scene)
      (place-image SQUARE-IMG x y scene))
    
    ; toy-x: -> Int
    ; toy-y: -> Int
    ; GIVEN: no arguments
    ; RETURNS: the x and y positions of this StatefulToy<%>
    ; EXAMPLES:
    ; square1 -> (new SquareToy% [x 200] [y 300] [d RIGHT] [speed 5])
    ; (send square1 toy-x) -> 200
    ; (send square1 toy-y) -> 300
    (define/public (toy-x)
      x)
    (define/public (toy-y)
      y)
    
    ; toy-color: -> ColorString
    ; GIVEN: no arguments
    ; RETURNS: the current color of this StatefulToy<%>
    ; EXAMPLES:
    ; square1 -> (new SquareToy% [x 200] [y 300] [d RIGHT] [speed 5])
    ; (send square1 toy-color) -> "blue"
    (define/public (toy-color)
      SQUARE-COLOR)
    
    ; for-test:get-direction : -> Direction
    ; GIVEN: no arguments
    ; RETURNS: the current direction of this StatefulToy<%>
    ; EXAMPLES:
    ; square1 -> (new SquareToy% [x 200] [y 300] [d RIGHT] [speed 5])
    ; (send square1 for-test:get-direction) -> "right"
    (define/public (for-test:get-direction)
      d)
    
    ; for-test:get-speed: -> PosInt
    ; GIVEN: no arguments
    ; RETURNS: the speed of this StatefulToy<%>
    ; EXAMPLES:
    ; square1 -> (new SquareToy% [x 200] [y 300] [d RIGHT] [speed 5])
    ; (send square1 for-test:get-speed:) -> 5
    (define/public (for-test:get-speed)
      speed)))
;------------------------------------------------------------------------------;
; CircleToy% -- a class that satisfies the StatefulToy<%> interface
; A CircleToy is a (new CircleToy% [x Int] 
;                                  [y Int]
;                                  [color ColorString]
;                                  [tick-count NonNegInt])
; Interpretation: represents a circle toy where
; x: x-position of the center of the circle toy
; y: y-position of the center of the circle toy
; color: color of the circle toy - it is either red or green
; tick-count: the number of ticks for which the circle toy has had the color
(define CircleToy%
  (class* object% (StatefulToy<%>)
    (init-field x)  ; x-position of the center of the circle toy
    (init-field y)  ; y-position of the center of the circle toy
    (init-field color) ; color of the circle toy
    (init-field tick-count) ; the number of ticks for which the 
    ;                         circle toy has had the color
    
    ; image of the red circle
    (field [RED-CIRCLE-IMG (circle CIRCLE-TOY-RADIUS SOLID RED)])
    ; image of the green circle
    (field [GREEN-CIRCLE-IMG (circle CIRCLE-TOY-RADIUS SOLID GREEN)])
    ; the initial value of tick count
    (field [TICK-START 1])
    ; the final value of tick count
    (field [TICK-END 5])
    
    (super-new)
    
    ; on-tick: -> Void
    ; GIVEN: no arguments
    ; EFFECT: the state of this StatefulToy<%> that should follow a tick.
    ; EXAMPLES:
    ; circle1 -> (new CircleToy% [x 100] [y 200] [color GREEN] [tick-count 5])
    ; circle2 -> (new CircleToy% [x 100] [y 100] [color RED] [tick-count 3])
    ; (send circle1 on-tick) -> the stateful toy updated such that the color
    ;                           is RED and the tick-count is reset to 1.
    ; (send circle2 on-tick) -> the stateful toy updated by adding 1 to the
    ;                           tick-count.
    (define/public (on-tick)
      (if (= tick-count TICK-END)
          (begin
            (set! color (send this change-color))
            (set! tick-count TICK-START))
          (set! tick-count (+ 1 tick-count))))
    
    ; add-to-scene: Scene -> Scene
    ; GIVEN: no arguments
    ; RETURNS: a Scene like the given one, but with this StatefulToy<%>
    ;          drawn on it.
    ; WHERE: the color of StatefulToy<%> is either RED or GREEN
    ; EXAMPLES:
    ; circle1 -> (new CircleToy% [x 100] [y 200] [color GREEN] [tick-count 5])
    ; circle2 -> (new CircleToy% [x 100] [y 100] [color RED] [tick-count 5])
    ; scene1 -> EMPTY-CANVAS
    ; scene2 -> (place-image GREEN-CIRCLE-IMG 100 200 scene1)
    ; scene3 -> (place-image RED-CIRCLE-IMG 100 100 scene2)
    ; (send circle1 add-to-scene scene1) -> scene2
    ; (send circle2 add-to-scene scene2) -> scene3
    (define/public (add-to-scene scene)
      (if (string=? color RED)
          (place-image RED-CIRCLE-IMG x y scene)
          (place-image GREEN-CIRCLE-IMG x y scene)))
    
    ; change-color: -> ColorString
    ; GIVEN: no arguments
    ; RETURNS: the color red if the color of this StatefulToy<%> is green, 
    ;          the color green if the color of this StatefulToy<%> is red
    ; WHERE: the color of StatefulToy<%> is either RED or GREEN
    ; EXAMPLES:
    ; circle1 -> (new CircleToy% [x 100] [y 200] [color GREEN] [tick-count 5])
    ; circle2 -> (new CircleToy% [x 100] [y 100] [color RED] [tick-count 5])
    ; (send circle1 change-color) -> "red"
    ; (send circle2 change-color) -> "green"
    (define/public (change-color)
      (if (string=? color RED)
          GREEN
          RED))
    
    ; toy-x: -> Int
    ; toy-y: -> Int
    ; GIVEN: no arguments
    ; RETURNS: the x and y positions of this StatefulToy<%>
    ; EXAMPLES:
    ; circle1 -> (new CircleToy% [x 100] [y 200] [color GREEN] [tick-count 0])
    ; (send circle1 toy-x) -> 100
    ; (send circle1 toy-y) -> 200
    (define/public (toy-x)
      x)
    (define/public (toy-y)
      y)
    
    ; toy-color: -> ColorString
    ; GIVEN: no arguments
    ; RETURNS: the current color of this StatefulToy<%>
    ; EXAMPLES:
    ; circle1 -> (new CircleToy% [x 100] [y 100] [color GREEN] [tick-count 0])
    ; (send circle1 toy-color) -> "green"
    (define/public (toy-color)
      color)
    
    ; for-test:get-tick-count: -> NonNegInt
    ; GIVEN: no arguments
    ; RETURNS: the current color of this StatefulToy<%>
    ; EXAMPLES:
    ; circle1 -> (new CircleToy% [x 100] [y 100] [color GREEN] [tick-count 0])
    ; (send circle1 for-test:get-tick-count:) -> 0
    (define/public (for-test:get-tick-count)
      tick-count)))
;------------------------------------------------------------------------------;

; make-world : PosInt -> World%
; GIVEN: the speed (in pixels/tick)
; RETURNS: a world with a target, but no toys, and in which any
;          toys created in the future will travel at the given 
;          speed (in pixels/tick).
; EXAMPLE:
; (make-world 5) -> (new World% [toys empty] [speed 5]
;       [x 200] [y 250] [selected? false] [target-mx 0] [target-my 0])
; STRATEGY: Function compostion
(define (make-world speed)
  (new World%
       [toys empty]
       [speed speed]
       [x TARGET-INITIAL-X]
       [y TARGET-INITIAL-Y]
       [selected? false] 
       [target-mx ZERO] 
       [target-my ZERO]))
;------------------------------------------------------------------------------;
; make-square-toy : Int Int PosInt -> SquareToy%
; GIVEN: an x and a y position, and a speed (in pixels/tick)
; RETURNS: an object representing a square toy at the given position,
;          travelling right at the given speed.
; EXAMPLE:
; (make-square-toy 100 200 5) -> (new SquareToy% [x 100] [y 200] 
;                                 [d RIGHT] [speed 5]))
; STRATEGY: Function composition
(define (make-square-toy x y speed)
  (new SquareToy% [x x] [y y] [d RIGHT] [speed speed]))
;------------------------------------------------------------------------------;
; make-circle-toy : Int Int -> CircleToy%
; GIVEN: an x and a y position
; RETURNS: an object representing a circle toy at the given position,
;          that has green color.
; EXAMPLE:
; (make-circle-toy 140 150) -> (new CircleToy% [x 140] [y 150] [color "green"] 
;                                      [tick-count 1])
; STRATEGY: Function composition
(define (make-circle-toy x y)
  (new CircleToy% [x x] [y y] [color GREEN] [tick-count ONE]))
;------------------------------------------------------------------------------;
; run : PosNum PosInt -> World%
; GIVEN: a frame rate (in seconds/tick) and a square-speed (in pixels/tick),
;        creates and runs a world.  
; RETURNS: the final state of the world.
(define (run rate speed)
  (big-bang (make-world speed)
            (on-tick
             ; World% -> Void
             ; GIVEN: a world
             ; EFFECT: the stateful world updated to the state that it 
             ;         should be in after a tick.
             (lambda (w) (send w on-tick) w)
             rate)
            (on-draw
             ; World% -> Scene
             ; GIVEN: a world
             ; RETURNS: the world drawn on the given scene
             (lambda (w) (send w on-draw)))
            (on-key
             ; World% -> Void
             ; GIVEN: a world
             ; RETURNS: the stateful world updated to the state that it 
             ;         should be in after the given key event
             (lambda (w kev) (send w on-key kev) w))
            (on-mouse
             ; World% -> Void
             ; GIVEN: a world
             ; RETURNS: the stateful world updated to the state that it 
             ;         should be in after the given mouse event
             (lambda (w x y evt) (send w on-mouse x y evt) w))))

;------------------------------------------------------------------------------;
; TESTING FRAMEWORK
;------------------------------------------------------------------------------;
; Constants for tests
(define FIVE 5)
(define INIT-X 200)
(define INIT-Y 250) 
(define INIT-SPEED 10)

; circles-equal?: ListOfCircleToy% ListOfCircleToy% -> Boolean
; GIVEN: two lists of circle toys
; RETURNS: true iff the given lists of circle toys are equal else returns false
; EXAMPLES: see tests below
(define (circles-equal? l1 l2)
  (andmap
   ; CircleToy% CircleToy% -> Boolean
   ; GIVEN: two circle toys
   ; RETURNS: true iff the given circle toys are equal else returns false
   (lambda (t1 t2) (circle-equal? t1 t2))
   l1 l2))

; circle-equal?: CircleToy% CircleToy% -> Boolean
; GIVEN: two circle toys
; RETURNS: true iff the given circle toys are equal else returns false
; EXAMPLES: see tests below
(define (circle-equal? c1 c2)
  (and
   (= (send c1 toy-x) (send c2 toy-x)) 
   (string=? (send c1 toy-color) (send c2 toy-color))
   (= (send c1 toy-y) (send c2 toy-y))
   (= (send c1 for-test:get-tick-count) (send c2 for-test:get-tick-count))))

; square-equal?: SquareToy% SquareToy% -> Boolean
; GIVEN: two square toys
; RETURNS: true iff the given square toys are equal else returns false
; EXAMPLES: see tests below
(define (square-equal? s1 s2)
  (and
   (= (send s1 toy-x) (send s2 toy-x))
   (= (send s1 toy-y) (send s2 toy-y))
   (string=? (send s1 for-test:get-direction) (send s2 for-test:get-direction))
   (= (send s1 for-test:get-speed) (send s2 for-test:get-speed))))

(begin-for-test
  ; check if the initial world has parameters initialized to the desired values
  (check-equal? (send (make-world TEN) get-toys) empty
                "the list of toys should be empty initially")
  (check-equal? (send (make-world TEN) get-speed) 10
                "the speed should be initialized to the given argument")
  (check-equal? (send (make-world TEN) target-x) 200
                "the target center should be initialized to the center of the 
                 canvas")
  (check-equal? (send (make-world TEN) target-y) 250
                "the target center should be initialized to the center of the 
                 canvas")
  (check-equal? (send (make-world TEN) target-selected?) false
                "the target should be unselected initially")
  (check-equal? (send (make-world TEN) get-target-mx) 0
                "the mouse position should be initialized to 0")
  (check-equal? (send (make-world TEN) get-target-my) 0
                "the mouse position should be initialized to 0")
  ; check if the initial square has parameters initialized to desired values
  (check-equal? (send (make-square-toy INIT-X INIT-Y INIT-SPEED) toy-x)
                200
                "the square should be created with its center at the given
                 coordinate")
  (check-equal? (send (make-square-toy INIT-X INIT-Y INIT-SPEED) toy-y)
                250
                "the square should be created with its center at the given
                 coordinate")
  (check-equal? (send (make-square-toy INIT-X INIT-Y INIT-SPEED) toy-x)
                200
                "the square should be created with its center at the given
                 coordinate")
  (check-equal? (send (make-square-toy INIT-X INIT-Y INIT-SPEED)
                      for-test:get-speed)
                10
                "the square should be created with its initial speed as the 
                 given speed")
  (check-equal? (send (make-square-toy INIT-X INIT-Y INIT-SPEED)
                      for-test:get-direction)
                RIGHT
                "the square should be created with its initial direction
                 as the given direction")
  (check-equal? (send (make-square-toy INIT-X INIT-Y INIT-SPEED)
                      toy-color) "blue"
                "the color of square is always blue")
  (check-equal? (send (make-circle-toy INIT-X INIT-Y) toy-x)
                200
                "the circle should be created with its center at the given
                 coordinate")
  (check-equal? (send (make-circle-toy INIT-X INIT-Y) toy-y)
                250
                "the circle should be created with its center at the given
                 coordinate")
  (check-equal? (send (make-circle-toy INIT-X INIT-Y) toy-color)
                "green"
                "the circle should be created with its initial color as green")
  (check-equal? (send (make-circle-toy INIT-X INIT-Y) for-test:get-tick-count)
                ONE
                "the circle should be created with its initial tick-count=1")
  ; Testing mouse events
  (local
    ((define square-1 (new SquareToy% [x 200] [y 250] [d RIGHT] [speed 10]))
     (define world-1 (new World% [toys (list square-1)] [speed 10]
                          [x 200] [y 250] [selected? false] 
                          [target-mx 0] [target-my 0])))
    
    (send world-1 on-mouse 205 245 BUTTON-DOWN)
    (check-equal? (send world-1 get-target-mx)
                  205
                  "the target-mx is set to the x coordiante of mouse")
    (check-equal? (send world-1 get-target-my)
                  245
                  "the target-my is set to the y coordiante of mouse")
    (check-equal? (send world-1 target-selected?)
                  true
                  "the target should get selected on button down insie it")
    (send world-1 on-mouse 100 100 DRAG)
    (check-equal? (send world-1 target-x)
                  95
                  "the target centre changes with respect to the new
                   mouse position")
    (check-equal? (send world-1 target-y)
                  105
                  "the target centre changes with respect to the new
                   mouse position")
    (check-equal? (send world-1 get-target-mx)
                  100
                  "the mouse position within the target is set to the
                   actual mouse posiiton")
    (check-equal? (send world-1 get-target-my)
                  100
                  "the mouse position within the target is set to the
                   actual mouse posiiton")
    (send world-1 on-mouse 90 100 BUTTON-UP)
    (check-equal? (send world-1 target-selected?)
                  false
                  "the target should get unselected on button up inside it")
    (send world-1 on-mouse 10 10 "leave")
    (check-equal? (send world-1 target-x)
                  95
                  "the target centre remains unchanged")
    (check-equal? (send world-1 target-y)
                  105
                  "the target centre remains unchanged")
    (check-equal? (send world-1 get-target-mx)
                  100
                  "the mouse position within the target remains unchanged")
    (check-equal? (send world-1 get-target-my)
                  100
                  "the mouse position within the target remains unchanged")
    (check-equal? (send world-1 target-selected?)
                  false
                  "the target should remain unchanged on button up outside it")
    (send world-1 world-after-drag 10 100)
    (check-equal? (send world-1 target-x)
                  95
                  "the target centre remains unchanged")
    (check-equal? (send world-1 target-y)
                  105
                  "the target centre remains unchanged")
    (check-equal? (send world-1 get-target-mx)
                  100
                  "the mouse position within the target remains unchanged")
    (check-equal? (send world-1 get-target-my)
                  100
                  "the mouse position within the target remains unchanged")
    (send world-1 world-after-button-up)
    (check-equal? (send world-1 target-selected?)
                  false
                  "the target should remain unchanged on button up outside it")
    )
  ; testing key events
  (local
    ((define circle-2 (new CircleToy% [x 200] [y 250] 
                           [color GREEN] [tick-count 1]))
     (define circle-2-after-tick (new CircleToy% [x 200] [y 250] 
                                      [color GREEN] [tick-count 2]))
     (define square-2 (new SquareToy% [x 200] [y 250] [d RIGHT] [speed 5]))
     (define square-2-after-tick (new SquareToy% [x 205] 
                                      [y 250] [d RIGHT] [speed 5]))
     (define world-2 (new World%
                          [toys empty] [speed 5]
                          [x 200] [y 250] [selected? false] 
                          [target-mx 0] [target-my 0])))
    (send world-2 on-key "c")
    (check-equal? (circles-equal? (send world-2 get-toys)
                                  (list circle-2))
                  true
                  "the world should have one circle toy")
    (send world-2 on-key "s")
    (check-equal? (circle-equal? (second (send world-2 get-toys))
                                 circle-2)
                  true
                  "the world should retain the first circle toy")
    (check-equal? (square-equal? (first (send world-2 get-toys))
                                 square-2)
                  true
                  "the world should add a square toy")
    (check-equal? (length (send world-2 get-toys)) 2
                  "there are two toys in the world")
    (send world-2 on-key "t")
    (check-equal? (length (send world-2 get-toys)) 2
                  "no toy is added to the world for any other key event")
    (send world-2 on-tick)
    (check-equal? (length (send world-2 get-toys)) 2
                  "there are two toys in the world")
    (check-equal? (circle-equal? (second (send world-2 get-toys))
                                 circle-2-after-tick)
                  true
                  "the circle toy in the world should add 1 to its tick-count")
    (check-equal? (square-equal? (first (send world-2 get-toys))
                                 square-2-after-tick)
                  true
                  "the square toy should move to the right by 5 pixels"))
  ; Test on-draw method
  (local
    ((define circle-toy-at-target (new CircleToy% [x TARGET-INITIAL-X]
                                       [y TARGET-INITIAL-Y]
                                       [color GREEN]
                                       [tick-count ONE]))
     (define init-world-after-key-c (new World% 
                                         [toys (list circle-toy-at-target)]
                                         [speed TEN]    
                                         [x TARGET-INITIAL-X]
                                         [y TARGET-INITIAL-Y]
                                         [selected? false] [target-mx 
                                                            ZERO] 
                                         [target-my ZERO])))
    (check equal? (send init-world-after-key-c on-draw) 
           (place-image (circle CIRCLE-TOY-RADIUS SOLID GREEN) TARGET-INITIAL-X
                        TARGET-INITIAL-Y 
                        (place-image (circle TEN OUTLINE BLACK) 
                                     TARGET-INITIAL-X 
                                     TARGET-INITIAL-Y
                                     EMPTY-CANVAS))
           "the world should be drawn on the canvas"))
  ; Test square toys
  (local
    ((define square-1 (new SquareToy% [x 100] [y 300] [d LEFT] [speed 5]))
     (define square-2 (new SquareToy% [x 100] [y 300] [d RIGHT] [speed 5]))
     (define square-3 (new SquareToy% [x 15] [y 300] [d LEFT] [speed 5]))
     (define square-4 (new SquareToy% [x 395] [y 300] [d RIGHT] [speed 5]))
     (define square-5 (new SquareToy% [x 22] [y 300] [d LEFT] [speed 5]))
     (define square-6 (new SquareToy% [x 378] [y 300] [d RIGHT] [speed 5])))
    ; square moves one tick to left
    (send square-1 on-tick)
    (check-equal? (send square-1 toy-x) 95
                  "the x position of the center is reduced by speed")
    ; square moves one tick to right
    (send square-2 on-tick)
    (check-equal? (send square-2 toy-x) 105
                  "the x position of the center is increased by speed")
    ; square moves one tick left and goes beyond boundary
    (send square-5 on-tick)
    (check-equal? (send square-5 toy-x) 20
                  "the x position is set to the left-boundary")
    (check-equal? (send square-5 for-test:get-direction) RIGHT
                  "the direction is changed to right")
    ; square is outside the left boundary
    (send square-3 on-tick)
    (check-equal? (send square-3 toy-x) 20
                  "the x position is set to the left-boundary")
    (check-equal? (send square-3 for-test:get-direction) RIGHT
                  "the direction is changed to right")
    ; square moves one tick right and goes beyond boundary
    (send square-6 on-tick)
    (check-equal? (send square-6 toy-x) 380
                  "the x position is set to the right-boundary")
    (check-equal? (send square-6 for-test:get-direction) LEFT
                  "the direction is changed to left")
    ; square is outside the right boundary
    (send square-4 on-tick)
    (check-equal? (send square-4 toy-x) 380
                  "the x position is set to the right-boundary")
    (check-equal? (send square-4 for-test:get-direction) LEFT
                  "the direction is changed to left"))
  
  ; Test for add-to-scene function in square class
  (local
    ((define init-square-toy (new SquareToy% [x INIT-X] [y INIT-Y] [d RIGHT] 
                                  [speed INIT-SPEED])))
    (check equal? (send init-square-toy add-to-scene EMPTY-CANVAS)
           (place-image (square SQUARE-TOY-SIDE OUTLINE BLUE)
                        INIT-X INIT-Y EMPTY-CANVAS)
           "the square toy should be drawn on the given scene"))
  ; Testing the circle toy functions
  (local
    ((define circle-1 (new CircleToy% [x 100] [y 200] [color GREEN] 
                           [tick-count 1]))
     (define circle-2 (new CircleToy% [x 100] [y 200] [color RED] 
                           [tick-count 1])))
    (send circle-1 on-tick)
    (send circle-1 on-tick)
    (check-equal? (send circle-1 for-test:get-tick-count) 3
                  "the tick-count of the toy is 3")
    (check-equal? (send circle-1 toy-color) GREEN
                  "the toy should be unchanged until 5 ticks")
    (send circle-1 on-tick)
    (send circle-1 on-tick)
    (send circle-1 on-tick)
    (check-equal? (send circle-1 for-test:get-tick-count) 1
                  "when the tick-count of the toy is 5 is it reset to 1")
    (check-equal? (send circle-1 toy-color) RED
                  "the toy should change color after 5 ticks")
    (send circle-2 on-tick)
    (send circle-2 on-tick)
    (send circle-2 on-tick)
    (check-equal? (send circle-2 for-test:get-tick-count) 4
                  "the tick-count of the toy is 4")
    (check-equal? (send circle-2 toy-color) RED
                  "the toy should be unchanged until 5 ticks")
    (send circle-2 on-tick)
    (send circle-2 on-tick)
    (check-equal? (send circle-2 for-test:get-tick-count) 1
                  "when the tick-count of the toy is 5 is it reset to 1")
    (check-equal? (send circle-2 toy-color) GREEN
                  "the toy should change color after 5 ticks"))
  ; testing add-to-scene of circle toy
  (local
    ((define init-circle-toy (new CircleToy% [x INIT-X] [y INIT-Y] 
                                  [color GREEN] 
                                  [tick-count ONE]))
     (define red-circle-toy (new CircleToy% [x INIT-X] [y INIT-Y] 
                                 [color RED] 
                                 [tick-count ONE])))
    ; Test for correct placement of the green circle toy on the given scene
    (check-equal? (send init-circle-toy add-to-scene EMPTY-CANVAS) 
                  (place-image (circle CIRCLE-TOY-RADIUS SOLID GREEN)  
                               INIT-X INIT-Y EMPTY-CANVAS)
                  "the green circle toy should be drawn on the given scene")
    
    ; Test for correct placement of the red circle toy on the given scene
    (check-equal? (send red-circle-toy add-to-scene EMPTY-CANVAS)
                  (place-image (circle CIRCLE-TOY-RADIUS SOLID RED)
                               INIT-X INIT-Y EMPTY-CANVAS)
                  "the red circle toy should be drawn on the given scene")))