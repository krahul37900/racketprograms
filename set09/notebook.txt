Date   Who       Start   Stop     Interruptions  Question  TimeOnTask     Comments
11/13  RK,SG     13:00   22:40     40+30+60+60      1         390         Defined all the classes and functions
=============================committing to git 11/13  22:50 ===============================
11/14  RK,SG     11:30   13:30         30           1          90         Corrected target-after-drag, fixed an error in on-tick of squaretoy and added contracts and purpose statements
=============================committing to git 11/14  13:36 ===============================
11/15  RK,SG     13:00   13:50          0           1          50         Removed the Target class and made one world
=============================committing to git 11/15  13:59 ===============================
11/15  RK,SG     14:15   15:00          0           1          45         Worked on UML diagram
=============================committing to git 11/15  15:00 ===============================
11/16  RK,SG     14:00   16:00         30           1          90         Corrected the contracts and purpose statements for all the functions
=============================committing to git 11/16  16:10 ===============================
11/16  RK,SG     19:00   22:40      10+20+10        1         180         Gave examples for all the methods
=============================committing to git 11/16  22:40 ===============================
11/17  RK,SG     08:00   13:25          0           1         325         Wrote test cases
=============================committing to git 11/17  13:28 ===============================
11/17  RK,SG     14:00   14:30          0           1          30         Program Review
=============================committing to git 11/17  13:28 ===============================
11/17  RK,SG     15:00   15:40          0           1          40         Program Review 2
=============================committing to git 11/17  13:40 ===============================
11/17  RK,SG     16:15   16:45          0           1          30         Added UML diagram and made small changes in CircleToy%
=============================committing to git 11/17  16:50 ===============================

Total Time On Task Q1 (minutes): 1270
TOTQ1(hours and tenths): 21.1